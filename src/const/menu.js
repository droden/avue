import { baseUrl } from '@/config/env'
export default [{
	title: "欢迎页面",
	href: `#/wel`,
	icon: 'el-icon-my-kehuguanli',
	online: true,
	children: [],
}, {
	title: "百度",
	href: `https://www.baidu.com`,
	icon: 'el-icon-my-kehuguanli',
	online: true,
	children: [],
}, {
	title: "360安全",
	href: `https://www.360.cn`,
	icon: 'el-icon-my-kehuguanli',
	online: true,
	children: [],
}]