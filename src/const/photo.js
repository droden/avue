export default [{
	label: "基本信息",
	value: '1',
	children: [{
		label: "身份证",
		value: '1-1',
		children:[],
	}, {
		label: "客户经理与借款人合影",
		value: '1-2',
		children:[],
	}, {
		label: "结婚证",
		value: '1-3',
		children:[],
	}, {
		label: "户口溥",
		value: '1-4',
		children:[],
	}],
}, {
	label: "资产信息",
	value: '2',
	children: [{
		label: "房产证或土地证（购房合同或缴款证明）",
		value: '2-1',
		children:[],
	}, {
		label: "宅基地",
		value: '2-2',
		children:[],
	}, {
		label: "行驶证",
		value: '2-3',
		children:[],
	}, {
		label: "其它资产",
		value: '2-4',
		children:[],
	}],
}, {
	label: "经营信息",
	value: '3',
	children: [{
			label: "个体工商户",
			value: '3-1',
			children: [{
					label: "营业执照和税务登记证",
					value: '3-1-1',
					children:[],
				}, {
					label: "经营场所",
					value: '3-1-2',
					children:[],
				}, {
					label: "银行交易流水",
					value: '3-1-3',
					children:[],
				}, {
					label: "经营许可证或批文（特种行业必传）",
					value: '3-1-4',
					children:[],
				}, {
					label: "水电及其他能耗清单",
					value: '3-1-5',
					children:[],
				}, {
					label: "厂房设备",
					value: '3-1-6',
					children:[],
				}, {
					label: "原材料",
					value: '3-1-7',
					children:[],
				}, {
					label: "主要产品",
					value: '3-1-8',
					children:[],
				}, {
					label: "存货",
					value: '3-1-9',
					children:[],
				}, {
					label: "购销合同",
					value: '3-1-10',
					children:[],
				}, {
					label: "承包或租赁合同",
					value: '3-1-11',
					children:[],
				}

			]
		},
		{
			label: "固定工作人员",
			value: '3-2',
			children:[{
				label: "单位工资证明",
				value: '3-2-1',
				children:[],
			}, {
				label: "银行交易流水",
				value: '3-2-2',
				children:[],
			}],
		},
		{
			label: "农户",
			value: '3-3',
			children:[{
				label: "经营场所等信息",
				value: '3-3-1',
				children:[],
			}, {
				label: "种植种类",
				value: '3-3-2',
				children:[],
			}, {
				label: "养殖种类",
				value: '3-3-3',
				children:[],
			}, {
				label: "承包合同或租赁合同",
				value: '3-3-4',
				children:[],
			}],
		},
		{
			label: "其它类别",
			value: '3-4',
			children:[{
				label: "主要收入来源",
				value: '3-4-1',
				children:[],
			}, {
				label: "银行交易流水",
				value: '3-4-2',
				children:[],
			}],
		}
	],
}, {
	label: "贷款流程信息",
	value: '4',
	children:[{
		label: "借款申请书",
		value: '4-1',
		children:[],
	}, {
		label: "调查报告",
		value: '4-2',
		children:[],
	}, {
		label: "征信报告电子版",
		value: '4-3',
		children:[],
	}, {
		label: "审查审批表",
		value: '4-4',
		children:[],
	}, {
		label: "借款合同",
		value: '4-5',
		children:[],
	}, {
		label: "保证合同",
		value: '4-6',
		children:[],
	}, {
		label: "抵质押合同",
		value: '4-7',
		children:[],
	}],
}, {
	label: "押品信息",
	value: '5',
	children:[{
		label: "押品权属证明",
		value: '5-1',
		children:[],
	}, {
		label: "押品照片",
		value: '5-2',
		children:[],
	}],
}, {
	label: "其他信息",
	value: '6',
	children:[],
}]